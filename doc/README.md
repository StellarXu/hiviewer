README v1.2

This code is used to: Inspect and show the fits image,  Overlap different images (or contours), Process the data cube preliminarily.

# 关于 hiviewer 模块
* 用于
  * 使用Python为cube出图，调出Fits文件的信息，叠加不同fits的图

* 包含
  * core.py                        主要的class和操作  (./hiviewer/)
  * overlap.py                     叠图的函数 (./hiviewer/)
  * HIsource_fio.py                椭圆拟合一个HI源的HIviewer简化版  (./hiviewer/)
  * auto_compare.py                一个可以通过终端自动画出图片的程序  (./hiviewer/)
  * compare_instance.py            与上一个对应的半自动版本程序  (./hiviewer/)
  * utils.py                       一些函数 (./hiviewer/)
  * Fits_example.ipynb             非常详细的notebook实例  (./doc/)
  * M33_FAST_cube_new.fits         FAST对M33 HI的一个数据cube，[点击这里下载](http://ddl.escience.cn/f/VRsj),提取码：b9oqyk
  * MESSIER_033_I_103aE_dss1.fits  Palomar天文台拍摄的光学波段的M33  (./data/)

* 需要的Python包：
    * Python3环境
    * numpy,matplotlib,astropy,spectral-cube
    * 重新投影需要montage_wrapper(它又需要sudo apt get montage)或者kapteyn或者reproject包
    * 寻找极值点位置需要skimage

* 如果有问题或者bug如何联系我？
  stellarxu@qq.com

# 安装
使用setup.py

  下载安装包后 ** 先 cd 切换到代码(setup.py)所在目录下 **
  * 安装
    ```
    python setup.py install -f
    ```
  * 卸载
    ```
    python -c 'import hiviewer;print(hiviewer.__file__)'
    ```
    找到安装后的文件夹，删除。

# 示例

## 准备

1. 在任意位置新建文件夹 test 
2. 复制core.py ，auto_compare.py，Fits_example.ipynb 到 test 文件夹下 
3. 在test下新建文件夹 data , fig，用来存放数据和图片，示例M33_FAST_cube_new.fits放在fast文件夹下
4. 在test文件夹下按下述操作直接运行脚本或者Fits_example.ipynb 即可。使用jupyter notebook再配合DS9使用更佳。所以建议先看ipynb的。

## 关于class FitsPic 
这个类的属性与方法
```
    Attributes & Properties
    ----------
    filename: str   Name of FITS file
    ndim:  dimension number of the data
    ctype: ['coor--pro']    Coordinate type  and  projection.
    crval: Coordinate value at reference point (one per axis).
    crpix: Array location of the reference point in pixels (one per axis)
    cuint: Units for stated coordinate axes
    cdelt: Coordinate increment at reference point
    velo:  spectral axis of the cube 
    fill_nans_data: fill Nans of the data with zero.
    beam:  resolution information of the telescope
    
    Methods
    ----------
    deg2pix:         ra,dec coordinates to pixel position 
    pix2deg:         pixel position to ra,dec coordinates
    velo2sel:        velocity(km/s) to selected slice number
    get/plot_slice():get a 2D slice of the cube / show the  slice image.
    get/plot_spec(): get/plot the spectrum of a certain point.
    plot_side_slice: show a cube slice from one side.
    slab_moment():   compute 3 moment maps.
    plot_contour:    plot the contour on a certain slice
    reproj_convolve_2D: astropy.convolve and reproject the 2D fits to target fits
    reproj_convolve_cube: SpectralCube.convolve and reproject the fits cube to target fits.
    center_levels :  Expirical contour levels for a 2D FitsPic central part.
    
```
以上以及overlap操作的具体说明请直接help（  ）。



## Fits_example.ipynb             
这是一个非常详细的notebook实例，你可以了解FitsPic类的各种属性和方法，以及如何使用此包。

主要功能有：
* 演示FitsPic类的基础属性和方法
* 制作moment maps
* 绘制截面，等高线图，查看谱线等
* 卷积与重新投影

```
from hiviewer import FitsPic
from hiviewer.overlap import overlap_img_contour,overlap_two_contours,two_contours_with_peak
```
如果按照前面的安装方法，是可以按照上面方法import的。如果安装不成功或者找不到路径，可以在同一目录下，临时添加路径
```
import sys
sys.path.append('../../../pkgs/hiviewer-master')#这是包的路径，例如前面的/test/路径
from hiviewer import *
```
脚本同理

画图的话，我强烈推荐jupyter notebook,除非你的服务器上没有，那么可以考虑终端内运行python脚本。因为core.py这个程序里有许多参数是可调的，有一些常用参数可以外部更改，其他的可以直接在下载路径里修改core.py这个脚本。例如
```
 fig = plt.figure(figsize=(10,10))#调整大小
 ax.set_xlabel('设置标签',fontsize=16)#标签字号
 ax.set_xlim(1400，1405)#设置范围
 ax.imshow(data,vmin=vmin_max[0],vmax=vmin_max[1],alpha=0.5,cmap='rainbow')#上下限、透明度、colormap
 ax.tick_params(labelsize=17) #轴上数字字号
```
等等，诸如此类的。

## Quick start: auto_compare.py
没有特意做接口，是因为这个脚本比较简单，灵活使用，可以实现两个fits moment 0 的比较。

INPUT：
```
file1 = './fast/M33_FAST_cube_new.fits'
name1 = 'fast'
data_unit1 = 'K'
file2 = '../../arbo/M33_arecibo.fits'
name2 = 'arbo'
data_unit2 = 'Jy/beam'
#从这里下载data
Arecibo data: <http://www.naic.edu/~ages/public_cubes/M33_local.fits.gz>
article: <http://adsabs.harvard.edu/abs/2016MNRAS.456..951K>
VLA data and article: <https://www.researchgate.net/publication/260025915_M33_HI_maps>

pdfname = './fig/M33_FAST_arecibo_compare.pdf'

v1 = -300 #slab cube velocity(km/s)
v2 = -50 
```
* 这里输入两个cube文件路径，file1和2。后面画图基本默认文件1是background。
* name1和2之后会作为图片或者文件名的一部分，建议不要太长。
* data_unit1和2主要是给等高线作参考的，这个问题后面说。
* 如果所有图片输出在一个pdf里，这是pdfname。
* v1,v2 是切开cube做moment maps的速度区间，对于M33是-300km/s～-50km/s

* 然后程序会比较二者的分辨率和像素（格点）大小，然后将他们统一成最低的分辨率和最大的格点比较moment 0。

* 需要说明的是画contour图如何选择合适的等高线，第一种方法是结合ds9选择；第二种是使用定义过的center_levels 这个方法，= the Max data - np.log(a,b,等高线数目))的逆序排列，a,b是根据目前数据试验出的经验的结果，输入单位（比如'K'）也是为了区分量级来选择不同的a，b,如果不合适你也可以自己选择不同的参数。有的时候遇到离奇的数据点就会有问题；第三种是自己构造线性/非线性/其他的levels，比如我在代码里提供好的一些M33的。



```
bk_m0.plot_contour(bk1levels,vmin_max = (0,1000),alpha=0.9,clabel=False,
                     cbar_label='[K]', picname =f'./fig/{name1}m0',save = False)
                                                    #dont't save a single png
pdf.savefig()#saved in the pdf
plt.close()
```
* 以画这个fastm0的图片加等高线为例，表示以bk1levels画contours，背景是moment0，image数值显示（0，1000）区间，透明度0.9，不显示等高线上数值，背景图片colorbar标签'K',最终图片名称'./fig/fastm0_contour.png'(_contour.png是含在操作里的)，不单独保存。这里并没有用到sel参数,因为2D的数据没必要select层数。所以只要指定层数，cube的一个slice也是可以如上操作的。具体示例可以参照notebook。
* 不单独保存，所以会存入pdf中。希望单独保存就是True好了



```
overlap_img_contour(bk_m1,bk_m0,levels=bk1levels,c='k',vmin_max=None,alpha=0.8,bar_label='km/s',radec_range = [22.7,24.1,30,31.5],
                    clabel=False,radec_range = None,save = False,picname =f'./fig/{name1}_m0_and_m1')
pdf.savefig()
plt.close()

overlap_two_contours(bk1,fo2,bklevels=bk1levels,folevels=fo2levels, radec_range = [22.7,24.1,30,31.5],
                     alphabk=0.5,c='orange',picname=f'./fig/{name1}_grey_{name2}_orange',save = False)                          
pdf.savefig()
plt.close()
```
* 这两个类似，在image上画contour和画两个叠加的contour。坐标系/轴与你输入的第一个相同。应用更广泛一点。这里限制了只显示M33中心区域，用‘radec_range‘限制的ra，dec范围。


* 再后面你可以添加你需要的任何操作，比如寻找极值点，抽出cube里的谱线等等。注意极值点的坐标总是难以完全重合，峰值和谷底比较总是peaks位置稍微准一点，然而这个并不常用。

* 最后你还可以修改pdf的信息，比如主题和作者，否则你会注意到Author栏会出现奇怪生物。

最后终端内运行
```
python auto_compare.py
```
即可


## compare_instance.py            
与notebook对应的半自动版本程序,如果明白了第一个，那么这就没什么用了。只是把FAST，aecibo和VLA三者同时对比。因为其实用auto_compare.py 两两对比也可。



# 如何找到多波段数据？
1. 推荐先到[NED，NASA Extragalactic Database](https://ned.ipac.caltech.edu/)上搜索一下,找一下Image里有哪些。
[images search](http://ned.ipac.caltech.edu/forms/images.html)
[data search](http://ned.ipac.caltech.edu/forms/data.html>)
在后面的两个链接里，可以在最后找到EXTERNAL ARCHIVES AND SERVICES for MESSIER 031 Help一栏，也会给出很多获得数据的sites，resources的连接，或者直接retrieve到.

当然也推荐[CDS](https://cds.u-strasbg.fr/)数据库里搜索也很好，例如寻找EBHIS的一些[数据](http://cdsarc.u-strasbg.fr/viz-bin/qcat?J/A+A/585/A41)

2. [Lambda,NASA](https://lambda.gsfc.nasa.gov/product/)给出了很多data product
比如[HI Surveys、连续谱的、偏振的](https://lambda.gsfc.nasa.gov/product/foreground/fg_diffuse.cfm)从中可以找到某些巡天、论文或者data下载路径，虽然他叫Legacy Archive for Microwave Background Data Analysis...

[heasarc,NASA](https://heasarc.gsfc.nasa.gov/)上也有很多，比如首页就有查询xamin

3. 特意寻找一些巡天，常用的比如
[ALFALFA](http://egg.astro.cornell.edu/alfalfa/data/index.php)Arecibo
[Arecibo Galaxy Environment Survey,AGES](http://www.naic.edu/~ages/)
[The VLA FIRST Survey](http://sundog.stsci.edu/)中性氢巡天
[The NRAO VLA Sky Survey ,NVSS](https://www.cv.nrao.edu/nvss/)他在首页里也提到了很多Other large-scale radio surveys which may be of interest include
[The Effelsberg-Bonn HI Survey (EBHIS)](https://astro.uni-bonn.de/~jkerp/index.php)或者[这个马普所网页](https://www.mpifr-bonn.mpg.de/pressreleases/2015/9)
[GALFA HI Data](https://purcell.ssl.berkeley.edu/)
帕克斯中性氢巡天[HIPASS](http://hipass.anu.edu.au/)与焦德雷班克中性氢巡天HIJASS等等
```
Table B.1 Comparison of major blind HI surveys
Survey	Area	Beam	Vmax	Vresa	ts	rmsb	Ndet	min MHI c	Ref
 	(deg2)	(arcmin)	(km/s)	(km/s)	(s)	(mJy)	 	(Msun)	 
AHISS	65	 3.3	-700 - 7400	16	var	0.7	65	1.9x106	1
ADBS	430	 3.3	-650 - 7980	34	12	3.6	265	9.9x106	2
WSRT	1800	49. 	-1000 - 6500	17	60	18	155	4.9x107	3
Nancay CVn	800	4 x 20	-350 - 2350	10	80	7.5	33	2.0x107	4
HIJASS	1115	12. 	-1000 - 10000d	18	400	13	222	3.6x107	5
HIJASS-VIR	32	12. 	500 - 2500	18	3500	4.	31	1.1x107	6
HIDEEP	60	15.5	-1280 - 12700	18	9000	3.2	173	8.8x106	7
HIZSS	1840	15.5	-1280 - 12700	27	200	15.	110	4.1x107	8
HICAT	21341	15.5	300 - 12700	18	450	13.	4315	3.6x107	9
HIPASS	 	15.5	300 - 12700	18	450	13.	(6000)	3.6x107	10
AUDS	0.4	 3.5	-960 - 47000e	TBD	70 × 3600	0.02	(40)	0.6x10 6	11
AGES	TBD	 3.5	-960 - 47000e	TBD	300	0.5	TBD	1.4x106	12
ALFALFA	7000	 3.5	-2000 - 18000	11	28	1.6	(30000)	4.4x106	
```

只是一部分，不全

4. 去搜索论文，看看有没有告诉你数据在哪。
比如我在ReseaechGate上找到的VLA的M33


end
2020/06/02
2020/12/03


