# -*- coding: utf-8 -*-
"""
Created on  April,27, 2020

@author: Xu Chen, Jilin University

This code is used to: Compare different Images, Overlap contours
    Input:filepaths.You should decide the convolve target and reproject target, and contour levels.
    You can add more commends in the end.     
    Output a pdf or single figures.
"""
import matplotlib as mpl
mpl.use('Agg')

from astropy.io import fits
from matplotlib import pyplot as plt
import numpy as np
from astropy import units as u
from astropy import wcs,log
import os

from hiviewer import *
from matplotlib.backends.backend_pdf import PdfPages
#################################################################
'''
#if it doesn't have beam information(resolution)...
from astropy.io import fits
hdl=fits.open('./fast/M33_FAST_cube.fits')
hd=hdl[0].header
print(hd)

hd['CTYPE3']='VELOCITY'
hd['BMAJ']=3/60
hd['BMIN']=3/60
hd['BPA']=0.0

fits.writeto('./fast/M33_FAST_cube_new.fits',hdl[0].data,hd,overwrite=True)
hdl.close()
'''

pdfname = './fig/M33_FAST_Arecibo_VLA_compare.pdf'
if os.path.exists(pdfname):
    print(f"{pdfname} already exsists.Delete it...")
    #os.remove(pdfname)


#data cubes
fastcube = FitsPic('./data/M33_FAST_cube_new.fits')
arbocube = FitsPic('../../arbo/M33_arecibo.fits')
#    Arecibo data: http://www.naic.edu/~ages/public_cubes/M33_local.fits.gz
#    and article: http://adsabs.harvard.edu/abs/2016MNRAS.456..951K
vlacube = FitsPic('../../VLA/M33_HI_12sec-area.fits')
#    VLA data and article: https://www.researchgate.net/publication/260025915_M33_HI_maps

print(fastcube.data.shape,arbocube.data.shape,vlacube.data.shape)

#slab moment0
fastcube.slab_moment(v1=-300,v2=-50,filepath = './data/M33_')
fastm0 = FitsPic('./data/M33_HI-moment0.fits')

#resolution and pixel size
print(fastcube.beam,fastcube.cdelt)
print(arbocube.beam,arbocube.cdelt)
print(vlacube.beam,vlacube.cdelt)

#convolve and reproject
fastcube.spcube_convolve_reproj(fastm0,arbocube,v1=-300,v2=-50,fitsname='./data/fast')
arbocube.spcube_convolve_reproj(fastm0,arbocube,v1=-300,v2=-50,fitsname='./data/arbo')
vlacube.spcube_convolve_reproj(fastm0,arbocube,v1=-300,v2=-50,fitsname='./data/vla')

#using these to compare
arbo = FitsPic('./data/arbo_convolve_reproj_result.fits')
fast =FitsPic('./data/fast_convolve_reproj_result.fits')
vla= FitsPic('./data/vla_convolve_reproj_result.fits')


print(fast.data.shape,arbo.data.shape,vla.data.shape)

pdf = PdfPages(pdfname)

#suitable contour levels
alevels=(158.4-np.logspace(1.83,2.09,23))[::-1]
flevels=(1120-np.logspace(1.9,2.8,23))[::-1]
vlevels=(1100-np.logspace(2.2,3.06,17))[::-1]
print(f"arbo levels:{alevels}")
print(f"fast levels:{alevels}")
print(f"vla levels:{vlevels}")

#overlap
overlap_two_contours(fast,arbo,bklevels=flevels,folevels=alevels, radec_range = [22.7,24.1,30,31.5],
                     alphabk=0.5,c='orange',picname='./fig/fast_grey_arbo_orange',save = False)
                                                      #dont't save a single png
pdf.savefig()#saved in the pdf
plt.close()


overlap_two_contours(fast,vla,bklevels=flevels,folevels=vlevels, radec_range = [22.7,24.1,30,31.5],
                     alphafo=0.6,c='red',picname='./fig/fast_grey_vla_red',save=False)
pdf.savefig()
plt.close()


#search and mark the peaks and valleys
two_contours_with_peaks(fast,arbo,min_distance=2,bklevels=flevels,folevels=alevels,radec_range = [22.7,24.1,30,31.5],
                        alphabk=0.4,picname='./fig/fast_grey_arbo_pink',save = False)
pdf.savefig()
plt.close()

#you can add other functions...

d = pdf.infodict() 
import datetime
d['ModDate'] = datetime.datetime.today()
d['Subject'] = 'Compare M33'
d['Keywords'] = 'FAST,Arecibo,VLA'
d['Author'] = 'a cute bear Knut'
d['Title'] = pdfname

pdf.close()
    
print(f" Pictures saved in {pdfname}")
